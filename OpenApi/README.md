## How To add open api docs to your project 

This is th e guide to add open api and Swagger ui to your project .

#### Steps 
1. Add these plugins to your project 


        id "com.github.johnrengelman.processes" version '0.5.0'
        id "org.springdoc.openapi-gradle-plugin" version '1.3.0'

2. Add this dependency to your project.


        implementation group: 'org.springdoc', name: 'springdoc-openapi-ui', version: '1.5.0'


3. Setup api docs in your application properties .



            springdoc:
                swagger-ui:
                    path: /swagger-ui.html
                    enabled: false
                api-docs:
                    path: /api/docs
                    groups:
                      enabled: true



4. In your build.gradle add this config and setup accordingly.



            openApi {
                    apiDocsUrl = "http://URL/api/docs"
                    outputDir = file("openApi")
                    outputFileName = "oas3.json"
                    forkProperties = "-Dspring.profiles.active=dev"
                }



5. Go and execute generateOpenApiDocs from gradle.


### How to generate Api and Dtos from open api generated file.

1. Add OpenApi specifications plugin to your Intellij.

2. restart , right click on openApi.json generated file , select Edit swaggerCodeggen .

3. setup location and download generator from remote.

4. select languge you want .

DONE 



