## How To use Intellij file templates

### Setup

 On intellij click on File -> Manage IDE Settings -> Import Setting 

select setting.zip file and import into your IED .

### Spring

Main template designed to create a Mode DTo Repository Service and controller for a name .
#### How To Use 
 Create Package in your project , pick a model name you want to use , right click on package select New and pick (Model Jpa Repository Dto Controller) , Type your class name (Start with Capital
 letter) . 

You can also use separate File templates to generate only controller or JpaRepository .



### React Typescript

Multiple file template included in Typescript :

1.Function component .

2.Class Component .

3.Unit test by Jest .

4.Story book by Story book .




