## How to Start new Project 

#### How To Create 

1. Use Intellj to generate spring project.
2. Only use version  2.3+ and try to ignore 2.4 as some parts of library changed regarding properties.
3. Add require library in install Process (Discovery client , Config client , web services , jpa if using database , db driver).

#### Setup for profiles "dev" and "docker" 
1. Remove application.properties 
2. Add bootstrap.yml file 


    
        server:
        port:
            ${PORT_NUMBER:8084}
        spring:
            profiles: dev
            application:
                name: <SERVICE_ID>
            cloud:
                config:
                  enabled: false
                discovery:
                  enabled: false
        ---
        spring:
            profiles: docker
            application:
                     name: <SERVICE_ID>
            cloud:
                config:
                    uri: http://configserver:8888
   
if you are using JPA , you need to implement db config as well for each profile: 

        datasource:
            url: jdbc:h2:mem:testdb;DB_CLOSE_ON_EXIT=FALSE
            username: admin
            password: dbPassword
            #Database Setup
        jpa:
            database-platform: org.hibernate.dialect.H2Dialect
            hibernate:
              ddl-auto: update
        
            properties:
                hibernate:
                    jdbc:
                      lob:
                        non_contextual_creation: true


 for docker profile
 
        datasource:
            url: jdbc:postgresql://${db_url}
            username: ${db_user}
            password: ${db_password} 
         

#### Adding Docker plugin 

Setup this step to build and push your image into a repository for live deployment.

* Edit your setting.gradle and add this at top of your file.

        pluginManagement {
            repositories {
                maven {
                    url "https://auraservices.jfrog.io/artifactory/dockergcloud/"
                }
                gradlePluginPortal()
            }
        }
     
* In your gradle.build file add this to your plugins (check the registry for latest version number )

    `id 'net.aura.dockergcloud' version '1.0.1'`
 
* Refresh your gradle, you will see dockergcloud category in gradle tasks.

* Add this into your gradle.build.


        dockerGcloud {
            versionNumber = version
        }


* Build image also creats Dockerfile and build the project.
* Run tak pushImage from gcloud plugin (It will execute buildImage tag live Image and pushImage). 




 