## How To publish Artifact to Maven registry (jfrog)

### Setup

1. Create Gradle java project , and create package for your artifact and project , src/main/java/net/example/myproject 


2. Replace java plugin with java-library and maven-publish into your  plugin.

3. create gradle.properties file 

4. include userName and password as property into gradle properties 

5. Setup publish  , populate this with project information 


     
         publishing {
          publications {
              mavenJava(MavenPublication) {
                  from components.java
                  pom {
                      name = ''
                      description = ''
                      url = 'http://www.sample.com'
      
                      licenses {
                          license {
                              name = 'The Apache License, Version 2.0'
                              url = 'http://www.apache.org/licenses/LICENSE-2.0.txt'
                          }
                      }
                      developers {
                          developer {
                              id = 'betwar'
                              name = 'A.H.Safaie'
                              email = 'a.safaie@y7mail.com'
                          }
                      }
                      scm {
                          developerConnection = 'scm:git:ssh://git-url.git'
                          url = 'git-url/master/'
                      }
                  }
              }
           }

 add this code to process push to maven registry: 


            
            repositories {
                    maven {
                        name = 'common'
                        credentials {
                            username "$userName"
                            password "$password"
                        }
                        url = "https://your-artifactory/url/"
                    }
                }
                